import pygame
import requests
import matplotlib.pyplot as plt

GRAY = (125, 125, 125)
LIGHT_BLUE = (64, 128, 255)
is_closed = 0
is_open = 0

def all_floats():
    for exp in range( -1024, 1024 ):
        for man in range( -(2**52), 2**52 ):
            yield (man/2**52)*2**exp


def all_circle( r=1, domain=all_floats ):
    for x in domain():
       for y in domain():
          if x**2 + y**2 == r**2:
              yield x, y


class Friend:
    def __init__(self, name, last_name, user_id, is_closed):
        self.name = name
        self.last_name = last_name
        self.user_id = user_id
        self.is_closed = is_closed
        self.list_of_friends = []

    def get_friends(self):
        return self.list_of_friends

    def add_friend(self, friend):
        self.list_of_friends.append(friend)

    def draw(self,x, y, window):
        if len(self.list_of_friends) == 0:
            if self.is_closed:
                pygame.draw.rect(window, GRAY, (x, y, 2, 2))
            else:
                pygame.draw.rect(window, LIGHT_BLUE, (x, y, 2, 2))
        else:
            for i in self.list_of_friends:
                a,b = all_circle(r=1)
                i.draw(a+x, b+y, window)

    def get_open_close_friends(self):
        if len(self.list_of_friends) == 0:
            if self.is_closed:
                return (0, 1)
            else:
                return (1, 0)
        else:
            open = 0
            closed = 0
            for i in self.list_of_friends:
                a,b = i.get_open_close_friends()
                open+=a
                closed+=b
        return open, closed

def define_list_of_friends():
    token = '044046b8044046b8044046b8530429929200440044046b858ebc7a558a013883dd15c09'
    my_id = '94057928'
    my_list_of_friends = []
    response = requests.get("https://api.vk.com/method/friends.get?v=5.92&user_id="+
                            my_id+"&access_token="+token+"&order=name&fields=online").json()
    for i in range(response['response']['count']):
        tmp = Friend(name=response['response']['items'][i]['first_name'],
                     last_name=response['response']['items'][i]['last_name'],
                     user_id=response['response']['items'][i]['id'],is_closed=False)
        my_list_of_friends.append(tmp)

    for i in my_list_of_friends:
        prev_response = requests.get("https://api.vk.com/method/friends.get?v=5.92&user_id="+
                                str(i.user_id)+"&access_token="+
                                token+"&order=name&fields=online").json()
        if('response' in prev_response):
                for j in range(prev_response['response']['count']):
                    try:
                        tmp = Friend(name=prev_response['response']['items'][j]['first_name'],
                                     last_name=prev_response['response']['items'][j]['last_name'],
                                     user_id=prev_response['response']['items'][j]['id'],
                                     is_closed=prev_response['response']['items'][j]['is_closed'])
                    except Exception as e:
                        tmp = Friend(name=prev_response['response']['items'][j]['first_name'],
                                     last_name=prev_response['response']['items'][j]['last_name'],
                                     user_id=prev_response['response']['items'][j]['id'],
                                     is_closed=True)
                        pass
                    i.add_friend(tmp)
    return my_list_of_friends

#window = pygame.display.set_mode((1280,720))
#pygame.display.set_caption("Hello, pygame")
#screen = pygame.Surface((1280,720))


done = True

#while done:
    #for e in pygame.event.get():
       # if e.type == pygame.QUIT:
           # done = False
    #pygame.display.flip()
    #pygame.time.delay(5)
print(100)
list_of_fr = define_list_of_friends()
print(1000)
for i in list_of_fr:
    is_o, is_c = i.get_open_close_friends()
    is_open+=is_o
    is_closed+=is_c
    #a, b = all_circle(r=2)
    #i.draw(a + 100, b + 100, window)
print(is_open, is_closed)



# Data to plot
labels = 'Open', 'Closed'
sizes = [is_open, is_closed]
colors = ['gold', 'yellowgreen']
explode = (0.1, 0)  # explode 1st slice

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=140)

plt.axis('equal')
plt.show()